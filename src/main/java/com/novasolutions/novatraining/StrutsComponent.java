package com.novasolutions.novatraining;

import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Target;

@Documented
@Inherited
@Target({ ElementType.TYPE })
@Component
@Scope("prototype")
public @interface StrutsComponent {

    @AliasFor(annotation = Component.class, attribute = "value")
    String value() default "";
}
