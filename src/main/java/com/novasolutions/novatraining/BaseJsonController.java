package com.novasolutions.novatraining;

import com.opensymphony.xwork2.Action;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public abstract class BaseJsonController implements Action {

    private GenericResponse<?> response;

    public GenericResponse<?> getResponse() {
        return response;
    }

    public void setResult(Object data) {
        response = new GenericResponse<>(data);
    }

    public void setError(Exception cause) {
        response = new GenericResponse<>(cause);
    }

    public final void doError(Exception cause) {
        setError(cause);
    }

    public abstract void doExecute();

    public final String execute() throws Exception {
        try {
            doExecute();
            return SUCCESS;
        }
        catch (Exception cause) {
            doError(cause);
            return ERROR;
        }
    }
}
