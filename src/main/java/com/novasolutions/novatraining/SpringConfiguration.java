package com.novasolutions.novatraining;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
public class SpringConfiguration {
        
    @Bean
    public FilterRegistrationBean strutsPrepareExecuteFilter() {
        FilterRegistrationBean strutsRegistration = new FilterRegistrationBean();
        strutsRegistration.setFilter(new StrutsPrepareAndExecuteFilter());
        strutsRegistration.setOrder(1);
        strutsRegistration.addUrlPatterns("*.do");
        strutsRegistration.setName("StrutsPrepareAndExecuteFilter");
        return strutsRegistration;
    }
}
