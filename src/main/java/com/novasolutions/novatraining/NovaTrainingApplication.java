package com.novasolutions.novatraining;

import com.novasolutions.novatraining.user.UserModuleConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class NovaTrainingApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(NovaTrainingApplication.class, SpringConfiguration.class, UserModuleConfiguration.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(new Object[] {
                NovaTrainingApplication.class,
                SpringConfiguration.class,
                UserModuleConfiguration.class
        }, args);
	}
}
