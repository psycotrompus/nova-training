package com.novasolutions.novatraining.user;

import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserDto fromEntityToDto(UserEntity ent) {
        return new UserDto(ent.getUserId(), ent.getUsername(), ent.getPassword(), ent.getDob(), ent.getDateRegistered(),
                ent.isActive(), ent.getFullname());
    }

    public UserEntity fromDtoToEntity(UserDto dto) {
        return new UserEntity(dto.getUserId(), dto.getUsername(), dto.getPassword(), dto.getDob(), dto.getDateRegistered(),
                dto.isActive(), dto.getFullname());
    }
}
