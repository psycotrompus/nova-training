package com.novasolutions.novatraining.user;

import com.novasolutions.novatraining.BaseJsonController;
import com.novasolutions.novatraining.StrutsComponent;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@StrutsComponent
class UserSearchController extends BaseJsonController {

    @Autowired
    private UserService userService;

    private String prefix;

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void doExecute() {
        List<UserDto> users = userService.findByUserStartsWith(prefix);
        setResult(users);
    }
}