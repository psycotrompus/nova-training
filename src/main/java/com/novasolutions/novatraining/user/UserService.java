package com.novasolutions.novatraining.user;

import java.util.List;

public interface UserService {

    List<UserDto> findByUserStartsWith(String prefix);

    void registerUser(UserDto user);
}
