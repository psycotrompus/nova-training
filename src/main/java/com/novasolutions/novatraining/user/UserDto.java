package com.novasolutions.novatraining.user;

import java.util.Date;

public class UserDto {

    private final Integer userId;

    private final String username;

    private final String password;

    private final Date dob;

    private final Date dateRegistered;

    private final boolean active;

    private final String fullname;

    public UserDto(Integer userId, String username, String password, Date dob, Date dateRegistered, boolean active, String fullname) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.dob = dob;
        this.dateRegistered = dateRegistered;
        this.active = active;
        this.fullname = fullname;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Date getDob() {
        return dob;
    }

    public Date getDateRegistered() {
        return dateRegistered;
    }

    public boolean isActive() {
        return active;
    }

    public String getFullname() {
        return fullname;
    }

    public static class Builder {

        private Integer userId;

        private String username;

        private String password;

        private Date dob;

        private Date dateRegistered;

        private boolean active;

        private String fullname;

        public Builder fromUserEntity(UserEntity user) {
            this.userId = user.getUserId();
            this.username = user.getUsername();
            this.password = user.getPassword();
            this.dob = user.getDob();
            this.dateRegistered = user.getDateRegistered();
            this.active = user.isActive();
            this.fullname = user.getFullname();
            return this;
        }

        public UserDto build() {
            return new UserDto(userId, username, password, dob, dateRegistered, active, fullname);
        }
    }
}
