package com.novasolutions.novatraining.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface UserDao extends JpaRepository<UserEntity, Integer> {
    
    List<UserEntity> findByUsernameStartingWith(String prefix);
}
