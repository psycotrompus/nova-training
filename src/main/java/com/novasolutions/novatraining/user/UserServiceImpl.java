package com.novasolutions.novatraining.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserMapper userMapper;

    @Transactional(readOnly = true)
    public List<UserDto> findByUserStartsWith(String prefix) {
        return userDao.findByUsernameStartingWith(prefix)
                .parallelStream()
                .map(userMapper::fromEntityToDto)
                .collect(toList());
    }

    @Transactional
    public void registerUser(UserDto user) {

    }
}
