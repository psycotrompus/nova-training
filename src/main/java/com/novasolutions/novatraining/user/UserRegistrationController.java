package com.novasolutions.novatraining.user;

import com.novasolutions.novatraining.BaseJsonController;
import com.novasolutions.novatraining.StrutsComponent;
import org.springframework.beans.factory.annotation.Autowired;

@StrutsComponent
class UserRegistrationController extends BaseJsonController {

    @Autowired
    private UserService userService;

    private UserDto user;

    public void setUser(UserDto user) {
        this.user = user;
    }

    public void doExecute() {
        userService.registerUser(user);
    }
}
