package com.novasolutions.novatraining.user;

import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "USERS", uniqueConstraints = {
        @UniqueConstraint(name = "users_username_uq", columnNames = { UserEntity.USERNAME })
}, indexes = {
        @Index(name = "users_fullname_ix", columnList = UserEntity.FULLNAME)
})
public class UserEntity implements Serializable {

    public static final String USERNAME = "username";

    public static final String FULLNAME = "fullname";

    private static final long serialVersionUID = 763427255854210629L;

    @Id
    @GeneratedValue
    private Integer userId;

    @Column(length = 200, unique = true, nullable = false)
    private String username;

    @Column(length = 200, unique = true, nullable = false)
    private String password;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dob;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRegistered;

    @Column
    private boolean active;

    @Column(length = 200)
    private String fullname;

    public UserEntity() {}

    public UserEntity(Integer userId, String username, String password, Date dob, Date dateRegistered, boolean active,
                      String fullname) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.dob = dob;
        this.dateRegistered = dateRegistered;
        this.active = active;
        this.fullname = fullname;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(Date dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null|| !(o instanceof UserEntity)) {
            return false;
        }
        UserEntity rhs = (UserEntity) o;
        return new EqualsBuilder()
                .append(this.userId, rhs.userId)
                .append(this.username, rhs.username)
                .append(this.dob, rhs.dob)
                .append(this.dateRegistered, rhs.dateRegistered)
                .append(this.fullname, rhs.fullname)
                .isEquals();
    }
}
