package com.novasolutions.novatraining;

import java.io.Serializable;

public class GenericResponse<T> implements Serializable {

    private static final long serialVersionUID = 5774061308173148025L;

    private long timestamp;

    private State state;

    private T data;

    private Exception exception;

    public GenericResponse(T data) {
        this.timestamp = System.currentTimeMillis();
        this.state = State.SUCCESS;
        this.data = data;
    }

    public GenericResponse(Exception exception) {
        this.timestamp = System.currentTimeMillis();
        this.state = State.FAILURE;
        this.exception = exception;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public State getState() {
        return state;
    }

    public T getData() {
        return data;
    }

    public Exception getException() {
        return exception;
    }

    public enum State {

        SUCCESS, FAILURE;
    }
}
