((window) => {

    let NOVA = window.NOVA;

    NOVA.bootstrap = () => {
        NOVA.config.modules.forEach((module) => {
            if (!NOVA[module].init) {
                console.error(`No init() method for module ${module}`);
            }
            else {
                NOVA[module].init();
            }
        });

        Ext.application({
            name: 'NOVA',
            extend: 'NOVA.app.Application',
            requires: [
                'NOVA.app.view.ApplicationView'
            ],
            mainView: 'NOVA.app.view.ApplicationView'
        });
    };

})(window);