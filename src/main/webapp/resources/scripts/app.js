((window, Ext) => {

    Ext.define('NOVA.app.Application',{
        extend: 'Ext.app.Application',
        namespace: 'NOVA',
        requires: [ 'NOVA.app.*' ]
    });

})(window, Ext);