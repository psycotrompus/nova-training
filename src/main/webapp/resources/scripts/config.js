((window, Ext) => {

    let nova = window.NOVA || {
        config: {
            contextPath: '/nova',
            modules: []
        },
        addModule: (module) => {
            if (!nova.config.modules.includes(module)) {
                nova.config.modules.push(module);
            }
        }
    };

    window.NOVA = nova;

    Ext.Loader.setConfig({
        enabled : true,
        scriptCharset : 'UTF-8',
        disableCaching: true
    });

})(window, Ext);