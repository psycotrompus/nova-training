((window, Ext) => {

    let nova = window.NOVA;

    nova.Application = nova.Application || {
        init: () => {
            Ext.Loader.addClassPathMappings({
                'NOVA.app': `${JS_PATH}/app/js`
            });
        }
    };

    nova.addModule('Application');

})(window, Ext);