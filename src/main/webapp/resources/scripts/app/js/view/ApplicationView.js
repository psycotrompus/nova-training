Ext.define('NOVA.app.view.ApplicationView', {
    requires: [
        'Ext.form.Panel',
        'NOVA.app.controller.ApplicationController'
    ],
    extend: 'Ext.form.Panel',
    xtype: 'NovaAppView',
    autoScroll: true,
    controller: 'ApplicationController',
    renderTo: 'container',
    items: [{
        xtype: 'panel',
        title: 'NOVA Training Application',
        width: '80%',
        height: '20%'
    }, {
        xtype: 'panel',
        width: '80%',
        height: '80%'
    }]
});